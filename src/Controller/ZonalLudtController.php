<?php

namespace Drupal\ludt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Config\ConfigFactory;

/**
 * Class ZonalLudtController.
 */
class ZonalLudtController extends ControllerBase {
/**
* @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $config;


  /**
   * Constructor.
   *
   * @param RequestStack $request_stack
   * @param ConfigFactory $config
   */
  public function __construct(RequestStack $request_stack, ConfigFactory $config) {
    $this->requestStack = $request_stack;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * GetDetails.
   *
   * @return string
   *   Return getDetails string.
   */
  public function getDetails() {
    $instanceKey = $this->requestStack->getCurrentRequest()->query->get('iesfile_abs_url');
    if (isset($instanceKey) && $instanceKey != '') {
      return new Response($this->getSingleZonalDetails($instanceKey));
    }
    else {
      return new JsonResponse([ 'data' => 'No parameter supplied.', 'method' => 'GET', 'status'=> 404]);
    }

  }

  /**
   * Return zonal details for a single ies file.
   * getSingleZonalDetails.
   *
   * @return string
   */
  private function getSingleZonalDetails($ies_file) {
    $fileName = basename($ies_file);
   return '{
    "CalculateFileName": "'.$fileName.'",
    "Photometry": [
      {
      "FileName": "'.$fileName.'",
      "FileUrl": "'.$ies_file.'",
      }
      ],'.$this->getStaticData(). '}';
  }

  /**
   * Get the static data
   */
  private function getStaticData() {
    $jsonData = $this->config->get('ludt.indoor_outdoor_luxiflux_form_settings')->get('ludt_zonal_settings_json_code');
    return '"Zone": '. $jsonData;
  }

}
