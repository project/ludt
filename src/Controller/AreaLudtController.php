<?php

namespace Drupal\ludt\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AreaLudtController.
 */
class AreaLudtController extends ControllerBase
{
  /**
   * @var RequestStack
   */
  private $requestStack;

  /**
   * @var RequestStack
   */
  private $config;

  /**
   * Constructor.
   *
   * @param RequestStack $request_stack
   * @param ConfigFactory $config
   */
  public function __construct(RequestStack $request_stack, ConfigFactory $config)
  {
    $this->requestStack = $request_stack;
    $this->config = $config;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('request_stack'),
      $container->get('config.factory')
    );
  }

  /**
   * GetDetails.
   *
   * @return string
   *   Return getDetails string.
   */
  public function getDetails()
  {
    $instanceKey = $this->requestStack->getCurrentRequest()->query->get('iesfile_abs_url');
//    $displayType = $this->requestStack->getCurrentRequest()->query->get('display_type');
    if (isset($instanceKey) && $instanceKey != '') {
      return new Response($this->getAreaDetails($instanceKey));
    } else {
      return new JsonResponse(['data' => 'No parameter supplied.', 'method' => 'GET', 'status' => 404]);
    }
  }

  /**
   * getAreaDetails.
   *
   * @return string
   *   Return getAreaDetails string.
   */
  private function getAreaDetails($ies_url)
  {
    $ies_file_name = basename($ies_url);
    $ies_file_html = '
        {
          "FileName": "' . $ies_file_name . '",
          "FileUrl": "' . $ies_url . '",
        },';
    $output = '{}';
    if (!empty($ies_url)) {
      $output = $this->getDefaultDisplay($ies_file_name, $ies_file_html);
    }
    return $output;
  }

  private function getDefaultDisplay($ies_file_name, $iesfilehtml = '')
  {
    $jsonData = $this->config->get('ludt.indoor_outdoor_luxiflux_form_settings')->get('ludt_area_settings_json_code');
    $output = '
        {
    "CalculateFileName": "' . $ies_file_name . '",
    "Photometry": [
        ' . $iesfilehtml . '
    ],
    "Layout": ' . $jsonData;
    return $output;
  }

}
