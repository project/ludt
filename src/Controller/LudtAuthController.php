<?php

namespace Drupal\ludt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LudtAuthController.
 */
class LudtAuthController extends ControllerBase {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * GetAuthentication.
   *
   * @return string
   *   Return Hello string.
   */
  public function getAuthentication() {
    if(!($this->getZonalAuthentication())) {
      return new JsonResponse($this->getZonalAuthentication());
    }
    else {
      return new Response($this->getZonalAuthentication());
    }

  }

  private function getZonalAuthentication() {
    $config = $this->configFactory->get('ludt.indoor_outdoor_luxiflux_form_settings');
    $zonalUrl = trim($config->get('ludt_zonal_settings_zonal_url'));
    $zonalUsername = trim($config->get('ludt_zonal_settings_username'));
    $zonalPassword = trim($config->get('ludt_zonal_settings_password'));
    $handle = curl_init();
    $options = array(
      CURLOPT_URL            => "https://v1-zonal-api.luxiflux.com/Authenticate",
      CURLOPT_POST           => true,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POSTFIELDS     => '{"Username": "'. $zonalUsername .'", "Password": "'. $zonalPassword .'"}'
    );
    curl_setopt_array($handle, $options);
    $response = curl_exec($handle);
    curl_close($handle);

    header("Access-Control-Allow-Origin: " . $zonalUrl);
    header("Access-Control-Allow-Methods: GET");
    header("Cache-Control: no-store");
    return $response;
  }



  /**
   * Get Outdoor authentication.
   */

  public function getAreaAuthentication() {
    if(!($this->authorizeArea())) {
      return new JsonResponse($this->authorizeArea());
    }
    else {
      return new Response($this->authorizeArea());
    }
  }

  /**
   * Authorize area
   */

  private function authorizeArea() {
    $config = $this->configFactory->get('ludt.indoor_outdoor_luxiflux_form_settings');
    $areaUrl = trim($config->get('ludt_area_settings_area_url'));
    $areaUsername = trim($config->get('ludt_area_settings_username'));
    $areaPassword = trim($config->get('ludt_area_settings_password'));
    $handle = curl_init();
    $options = array(
      CURLOPT_URL            => "https://v1-area-api.luxiflux.com/Authenticate",
      CURLOPT_POST           => true,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POSTFIELDS     => '{"Username": "'. $areaUsername .'", "Password": "'. $areaPassword .'"}'
    );
    curl_setopt_array($handle, $options);
    $response = curl_exec($handle);
    curl_close($handle);

    header("Access-Control-Allow-Origin: https://v1-area-tools.luxiflux.com");
    header("Access-Control-Allow-Methods: GET");
    header("Cache-Control: no-store");
    return $response;
  }

}
