<?php

namespace Drupal\ludt\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LudtSettings.
 */
class LudtSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ludt.indoor_outdoor_luxiflux_form_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ludt_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ludt.indoor_outdoor_luxiflux_form_settings');

//    Indoor/Zonal Config Settings
    $form['ludt_zonal_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Indoor/Zonal Tool Settings'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $form['ludt_zonal_settings']['ludt_zonal_settings_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('The authentication user name.'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('ludt_zonal_settings_username'),
    ];
    $form['ludt_zonal_settings']['ludt_zonal_settings_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The authentication password.'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('ludt_zonal_settings_password'),
    ];
    $form['ludt_zonal_settings']['ludt_zonal_settings_zonal_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Zonal(Indoor) URL'),
      '#description' => $this->t('The base URL for the luxiflux zonal API.'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('ludt_zonal_settings_zonal_url'),
    ];
//    $form['ludt_zonal_settings']['ludt_zonal_settings_base_url'] = [
//      '#type' => 'textfield',
//      '#title' => $this->t('Site Base URL'),
//      '#description' => $this->t('The site base URL.'),
//      '#maxlength' => 512,
//      '#size' => 64,
//      '#default_value' => $config->get('ludt_zonal_settings_base_url'),
//    ];
    $form['ludt_zonal_settings']['ludt_zonal_settings_json_code'] = array(
      '#type' => 'textarea',
      '#title' => $this
        ->t('Design Layout JSON Code'),
      '#description' => $this->t('Provide the JSON code for zonal design tool'),
      '#default_value' => $config->get('ludt_zonal_settings_json_code'),
    );

//    Output/Area Config Settings

    $form['ludt_area_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Outdoor/Area Tool Settings'),
      '#open' => FALSE,
      '#tree' => TRUE,
    ];
    $form['ludt_area_settings']['ludt_area_settings_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#description' => $this->t('The authentication user name.'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('ludt_area_settings_username'),
    ];
    $form['ludt_area_settings']['ludt_area_settings_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('The authentication password.'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('ludt_area_settings_password'),
    ];
    $form['ludt_area_settings']['ludt_area_settings_area_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Area(Outdoor) URL'),
      '#description' => $this->t('The base URL for the luxiflux area API.'),
      '#maxlength' => 512,
      '#size' => 64,
      '#default_value' => $config->get('ludt_area_settings_area_url'),
    ];
    $form['ludt_area_settings']['ludt_area_settings_json_code'] = array(
      '#type' => 'textarea',
      '#title' => $this
        ->t('Design Layout JSON Code'),
      '#description' => $this->t('Provide the JSON code for area design tool'),
      '#default_value' => $config->get('ludt_area_settings_json_code'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $zonal_values = $form_state->getValue('ludt_zonal_settings');
    $area_values = $form_state->getValue('ludt_area_settings');
    $this->config('ludt.indoor_outdoor_luxiflux_form_settings')
      ->set('ludt_zonal_settings_username', $zonal_values['ludt_zonal_settings_username'])
      ->set('ludt_zonal_settings_password', $zonal_values['ludt_zonal_settings_password'])
      ->set('ludt_zonal_settings_zonal_url', $zonal_values['ludt_zonal_settings_zonal_url'])
//      ->set('ludt_zonal_settings_base_url', $zonal_values['ludt_zonal_settings_base_url'])
      ->set('ludt_zonal_settings_json_code', $zonal_values['ludt_zonal_settings_json_code'])

      ->set('ludt_area_settings_username', $area_values['ludt_area_settings_username'])
      ->set('ludt_area_settings_password', $area_values['ludt_area_settings_password'])
      ->set('ludt_area_settings_area_url', $area_values['ludt_area_settings_area_url'])
      ->set('ludt_area_settings_json_code', $area_values['ludt_area_settings_json_code'])
      ->save();
  }
}
