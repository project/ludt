<?php

namespace Drupal\ludt\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 * Provides a 'LudtBlock' block.
 *
 * @Block(
 *  id = "ludt_block",
 *  admin_label = @Translation("Lighting Design Tool Block"),
 * )
 */
class LudtBlock extends BlockBase implements ContainerFactoryPluginInterface {

  //constants
  const LUDT_TYPE_FIELD_ID = "field_ludt_type";
  const LUDT_IES_FILE_NAME_FIELD_ID ="field_ludt_ies_file_name";
  const ZONAL_AUTH_URL = "/zonal/luxiflux-zonal-auth";
  const ZONAL_FILE_INPUT_URL = "/zonal/get-details";
  const AREA_AUTH_URL = "/area/luxiflux-area-auth";
  const AREA_FILE_INPUT_URL = "/area/get-details";

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->requestStack = $container->get('request_stack');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->requestStack->getCurrentRequest()->get('node');
    $build = [];
    $toolSettings = [];
    $config = $this->configFactory->get('ludt.indoor_outdoor_luxiflux_form_settings');
    $baseUrl = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
    if($node instanceof NodeInterface && $node->hasField(self::LUDT_TYPE_FIELD_ID)
      && $node->hasField(self::LUDT_IES_FILE_NAME_FIELD_ID)) {
      $field_ludt_type = isset($node->get(self::LUDT_TYPE_FIELD_ID)->getValue()[0]) && isset($node->get(self::LUDT_TYPE_FIELD_ID)->getValue()[0]['value']) ? $node->get(self::LUDT_TYPE_FIELD_ID)->getValue()[0]['value'] : '';
      $field_ies_file_name = $node->get(self::LUDT_IES_FILE_NAME_FIELD_ID)->getValue();
      $field_ies_file_name_count = count($field_ies_file_name);
      if($field_ies_file_name_count > 0 && $field_ludt_type != '') {
        $iesFiles = [];
        foreach($field_ies_file_name as $ies) {
          $iesFiles[] = $ies['value'];
        }
        if(count($iesFiles) > 0) {
          $iesFilesList = implode(',', $iesFiles);
            if(in_array($field_ludt_type, ['indoor'])) {
              $toolSettings['tool_button'] = $this->t('@ludtb_indoor_button_text', [
                '@ludtb_indoor_button_text' => $this->configuration['ludtb_indoor_button_text'],
              ]);
              $toolSettings['tool_button_text'] = trim($toolSettings['tool_button']->getArguments()['@ludtb_indoor_button_text']);
              $toolSettings['luxiflux_url'] = trim($config->get('ludt_zonal_settings_zonal_url'));
              $toolSettings['auth_url'] = self::ZONAL_AUTH_URL;
              if((strpos($toolSettings['auth_url'], "http://") === false) && (strpos($toolSettings['auth_url'], "https://") === false)) {
                $toolSettings['auth_url'] = $baseUrl.''.$toolSettings['auth_url'];
              }
              $toolSettings['input_url'] = self::ZONAL_FILE_INPUT_URL;
              if((strpos($toolSettings['input_url'], "http://") === false) && (strpos($toolSettings['input_url'], "https://") === false)) {
                $toolSettings['input_url'] = $baseUrl.''.$toolSettings['input_url'];
              }
              $toolSettings['base_url'] = trim($config->get('ludt_zonal_settings_base_url')) == '' ? $baseUrl : trim($config->get('ludt_settings_site_base_url'));
              $toolSettings['iesfiles'] = $iesFilesList;
            }
            else {
              // For Outdoor
              $toolSettings['tool_button'] = $this->t('@ludtb_outdoor_button_text', [
                '@ludtb_outdoor_button_text' => $this->configuration['ludtb_outdoor_button_text'],
              ]);
              $toolSettings['tool_button_text'] = trim($toolSettings['tool_button']->getArguments()['@ludtb_outdoor_button_text']);
              $toolSettings['luxiflux_url'] = trim($config->get('ludt_area_settings_area_url'));
              $toolSettings['auth_url'] = self::AREA_AUTH_URL;
              if((strpos($toolSettings['auth_url'], "http://") === false) && (strpos($toolSettings['auth_url'], "https://") === false)) {
                $toolSettings['auth_url'] = $baseUrl.''.$toolSettings['auth_url'];
              }
              $toolSettings['input_url'] = self::AREA_FILE_INPUT_URL;
              if((strpos($toolSettings['input_url'], "http://") === false) && (strpos($toolSettings['input_url'], "https://") === false)) {
                $toolSettings['input_url'] = $baseUrl.''.$toolSettings['input_url'];
              }
              $toolSettings['base_url'] = '';
              $toolSettings['iesfiles'] = $iesFilesList;
          }
        }
        $build['#theme'] = 'ludt';
        $build['#toolSettings']= $toolSettings;
        $build['#attached'] = [
          'library' => [
            'ludt/ludt_custom',
          ],
        ];
      }

    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {

    return [
      'ludtb_indoor_button_text' => 'Indoor Lighting Design Tool',
      'ludtb_outdoor_button_text' => 'Outdoor Lighting Design Tool',
      'ludtb_general_button_text' => 'Lighting Design Tool',
      'label_display' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    //$form = parent::blockForm($form, $form_state);
    $form['ludtb_indoor_button_text'] = [
      '#type' => 'textfield',
      '#title' => t('Products Indoor Lighting Tool Button Title'),
      '#default_value' => $this->t($this->configuration['ludtb_indoor_button_text']),
    ];
    $form['ludtb_outdoor_button_text'] = [
      '#type' => 'textfield',
      '#title' => t('Products Outdoor Lighting Tool Button Title'),
      '#default_value' => $this->t($this->configuration['ludtb_outdoor_button_text']),
    ];
    $form['ludtb_general_button_text'] = [
      '#type' => 'textfield',
      '#title' => t('Products General Lighting Tool Button Title'),
      '#default_value' => $this->t($this->configuration['ludtb_general_button_text']),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['ludtb_indoor_button_text'] = $form_state->getValue('ludtb_indoor_button_text');
    $this->configuration['ludtb_outdoor_button_text'] = $form_state->getValue('ludtb_outdoor_button_text');
    $this->configuration['ludtb_general_button_text'] = $form_state->getValue('ludtb_general_button_text');
  }

}
