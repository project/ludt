Luxiflux Design Tool Integrator

CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration

INTRODUCTION
------------
This module creates a block which links to Area or Zonal type of Design Tool

REQUIREMENTS
------------
None.

RECOMMENDED MODULES
-------------------
None

INSTALLATION
------------
 * Install as usual:
 See https://www.drupal.org/documentation/install/modules-themes/modules-8
 for further information.

CONFIGURATION
-------------
The content type should have following fields
1) field_ludt_type of type List Text with values indoor and outdoor
2) field_ludt_ies_file_name of type long plain text which will contain the IES file url

